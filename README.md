This helper classes and especially the trait TokenPreference is meant for any developer who wishes to save a single token for his app inside account mechanism of Android. As a step to incorporate later multiple tokens for multiple purposes etc.

Used in the following projects:

* [bman](http://bman.co)
* [Fcarrier](http://facebook.com/FcarrierApp)