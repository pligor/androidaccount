package com.pligor.androidaccount

import android.content.Context
import android.accounts._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://www.jiahaoliuliu.com/2012/05/android-account-manager-part-i.html
 * http://www.jiahaoliuliu.com/2012/06/android-account-manager-part-ii.html
 */
trait TokenPreference {

  protected def ACCOUNT_TYPE: String //bman.account.type

  //this must be unique among all apps

  /**
   * mAuthTokenType is the type of token that I request from the server.
   * I can have the server give me different tokens for read-only or full access to an account,
   * or even for different services within the same account.
   * A good example is the Google account, which provides several auth-token types:
   * “Manage your calendars”, “Manage your tasks”, “View your calendars” and more..
   * On this particular example I don’t do anything different for the various auth-token types.
   */
  //you should set the same string on the server
  protected def DEFAULT_TYPE_AUTH_TOKEN: String //auth.token

  //val EMPTY_TOKEN = "";

  protected def dummyUsername: String //bman

  def setValue(token: String)(implicit context: Context) = {
    val accountManager = getAccountManager

    val notSavingThePassword = ""

    val userData = null

    val account = new Account(dummyUsername, ACCOUNT_TYPE)

    val accountAlreadyExists = !accountManager.addAccountExplicitly(account, notSavingThePassword, userData)
    if (accountAlreadyExists) {
      accountManager.setPassword(account, notSavingThePassword)
    }

    accountManager.setAuthToken(account, DEFAULT_TYPE_AUTH_TOKEN, token)

    this
  }

  def isClear(implicit context: Context) = !getAccountOption.isDefined

  def clear(implicit context: Context): Unit = {
    val removeCallback = new AccountManagerCallback[java.lang.Boolean] {
      def run(futureBoolean: AccountManagerFuture[java.lang.Boolean]): Unit = {
        assert(futureBoolean.getResult, "if this happens too often maybe you should consider an asynchronous solution")
      }
    }

    val accountManager = getAccountManager

    getAccounts.foreach {
      account =>
        accountManager.removeAccount(account, removeCallback, null)
    }
  }

  private def getAccountOption(implicit context: Context) = getAccounts.headOption

  private def getAccounts(implicit context: Context) = {
    getAccountManager.getAccountsByType(ACCOUNT_TYPE)
  }

  private def getAccountManager(implicit context: Context) = AccountManager.get(context)

  def getValue(implicit context: Context/*, activity: Activity*/): Option[String] = {

    getAccountOption.map {
      account =>
        getAccountManager.peekAuthToken(account, DEFAULT_TYPE_AUTH_TOKEN)
    }

    /*getAccountOption.map({
      account =>
        log log "grabbing the auth token";
        val options = new Bundle;
        val handlerIsNullForMainThread = null;

        getAccountManager.getAuthToken(
          account,
          AccountConfig.DEFAULT_AUTH_TOKEN_TYPE,
          options,
          activity,
          callbackGetAuthToken,
          handlerIsNullForMainThread
        );
    }).getOrElse(() => None)*/
  }

  /*private val callbackGetAuthToken = new AccountManagerCallback[Bundle] {
    def run(futureBundle: AccountManagerFuture[Bundle]) {
      try {
        val bundle = futureBundle.getResult;
        log log "tried to get the auth token";

        if (bundle.isEmpty) {
          showDialogAndExit(getResources.getString(R.string.on_get_in_no_connection));
        } else {
          //available keys:
          //AccountManager.KEY_ACCOUNT_NAME
          //AccountManager.KEY_ACCOUNT_TYPE
          //AccountManager.KEY_AUTHTOKEN

          /*bundle.keySet().asScala foreach {key => log log "key: " + key;}*/

          val token = bundle.getString(AccountManager.KEY_AUTHTOKEN);

          log log "auth token is: " + token;

          val key = MyAuthenticatorActivity.OUTPUT_EXTRAS.STRING_FOR_EXIT.toString;
          if (bundle.containsKey(key)) {
            showDialogAndExit(bundle.getString(key));
          } else {
            authToken setValue token;
          }
        }
      } catch {
        case e: OperationCanceledException => {
          log log "OperationCanceledException";
          finish();
        }
        case e: AuthenticatorException => {
          log log "AuthenticatorException";
          finish();
        }
        case e: IOException => {
          log log "IOException";
          finish();
        }
      } finally {
        safeDismissProgressDialog();
      }
    }
  }*/
}
