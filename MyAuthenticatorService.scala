package com.pligor.androidaccount

import android.app.Service
import android.content.Intent
import android.os.IBinder

/*object MyAuthenticatorService {
  private var accountAuthenticator: AccountAuthenticator = null;
}*/

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyAuthenticatorService extends Service {
  def onBind(intent: Intent): IBinder = {
    if(intent.getAction == android.accounts.AccountManager.ACTION_AUTHENTICATOR_INTENT) {
      implicit val context = this

      new AccountAuthenticator().getIBinder
    } else {
      null
    }
  }

  /*private def getAuthenticator = {
    if(MyAuthenticatorService.accountAuthenticator == null) {
      MyAuthenticatorService.accountAuthenticator = new AccountAuthenticator(context);
    }
    MyAuthenticatorService.accountAuthenticator;
  }*/
}
